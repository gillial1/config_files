# Add the following lines to the end of your .bashrc file.
# Make sure to add a the $HOME/.bashrc.d directory.
# Inside this directory, you can place your custom configuration files.
# For example, you might have files like "01-custom-settings.sh," "02-aliases.sh," and so on.

# Load custom configurations from the ~/.bashrc.d directory
if [ -d "$HOME/.bashrc.d" ]; then
    for config_file in "$HOME/.bashrc.d"/*.sh; do
        if [ -r "$config_file" ]; then
            source "$config_file"
        fi
    done
fi

