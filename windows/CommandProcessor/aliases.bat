@echo off

:: Add key with name AutoRun to HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\CommandProcessor
:: containing the full path
:: Put this file in %USERPROFILE%\.config\cmd_window\

DOSKEY homedir=cd "C:\Users\gilli"
DOSKEY docdir=cd "C:\Users\gilli\Documents"
DOSKEY caddir=cd "F:\CAD"
DOSKEY desk=cd "C:\Users\gilli\Desktop"
DOSKEY ls=lsd -lahF $*
DOSKEY cat=type $*